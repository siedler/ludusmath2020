﻿using System;
using UnityEngine;

using UnityEngine.UI;

namespace AssemblyCSharp
{
	public class Configuracoes:MonoBehaviour
	{
		public Dropdown drop;
		public Image bt1;
		public Image bt2;
		public Image bt3;
		void Start()
		{
			this.atualizarBtnNivel ();
			//pega a orientação determinada ou atribui 0(Horizontal) como padrão 
			if (PlayerPrefs.HasKey ("orientacao")) {
				drop.value = PlayerPrefs.GetInt ("orientacao");
			} else {
				PlayerPrefs.SetInt("orientacao",0);
			}


		}
		//Orientação 0 -- Horizontal, 1 - Vertical
		public void trocarOrientacao()
		{
			 

			PlayerPrefs.SetInt("orientacao",drop.value);
		}
		private void atualizarBtnNivel()
		{
			int nivel = PlayerPrefs.GetInt ("nivel");
			switch (nivel) {
			case 1:
				bt1.sprite = Resources.Load<Sprite> ("Botoes/dif_91")as Sprite;
				bt2.sprite = Resources.Load<Sprite> ("Botoes/dif_95")as Sprite;
				bt3.sprite = Resources.Load<Sprite> ("Botoes/dif_97")as Sprite;
				break;
			case 2:
				bt1.sprite = Resources.Load<Sprite> ("Botoes/dif_93")as Sprite;
				bt2.sprite = Resources.Load<Sprite> ("Botoes/dif_89")as Sprite;
				bt3.sprite = Resources.Load<Sprite> ("Botoes/dif_97")as Sprite;
				break;
			case 3:
				bt1.sprite = Resources.Load<Sprite> ("Botoes/dif_93")as Sprite;
				bt2.sprite = Resources.Load<Sprite> ("Botoes/dif_95")as Sprite;
				bt3.sprite = Resources.Load<Sprite> ("Botoes/dif_87")as Sprite;
				break;
			default:
				bt1.sprite = Resources.Load<Sprite> ("Botoes/dif_91")as Sprite;
				bt2.sprite = Resources.Load<Sprite> ("Botoes/dif_95")as Sprite;
				bt3.sprite = Resources.Load<Sprite> ("Botoes/dif_97")as Sprite;
				break;
			}
		}

		public void trocarNivel(int nivel)
		{
			PlayerPrefs.SetInt ("nivel", nivel);
			atualizarBtnNivel ();

		}

	}
}

