﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Fases : MonoBehaviour {


    public GameObject botao;
    public bool teste;
    public int valor;
    // Use this for initialization
    public Canvas canvas;
    public GameObject EG;
    public GameObject EP1;
    public GameObject EP2;
    public GameObject FASE2ATIVA;
    public GameObject FASE2INATIVA;
    //public GameObject FASE2;
    public GameObject F2EG;
    public GameObject F2EP1;
    public GameObject F2EP2;
    public GameObject FASE3ATIVA;
    public GameObject FASE3INATIVA;
    public GameObject F3EG;
    public GameObject F3EP1;
    public GameObject F3EP2;
    public GameObject FASE4ATIVA;
    public GameObject FASE4INATIVA;
    public GameObject F4EG;
    public GameObject F4EP1;
    public GameObject F4EP2;
    public GameObject FASE5ATIVA;
    public GameObject FASE5INATIVA;
    public GameObject F5EG;
    public GameObject F5EP1;
    public GameObject F5EP2;
    public GameObject NOMEFASE;

    //teste estrelas por nivel de dificuldade 
    // MÉDIO

    //public GameObject F6EG;
    //public GameObject F6EP1;
    //public GameObject F6EP2;
    //public GameObject F7EG;
    //public GameObject F7EP1;
    //public GameObject F7EP2;
    //public GameObject F8EG;
    //public GameObject F8EP1;
    //public GameObject F8EP2;
    //public GameObject F9EG;
    //public GameObject F9EP1;
    //public GameObject F9EP2;
    //public GameObject F10EG;
    //public GameObject F10EP1;
    //public GameObject F10EP2;

    //DIFÍCIL

    public GameObject F11EG;
    public GameObject F11EP1;
    public GameObject F11EP2;
    public GameObject F12EG;
    public GameObject F12EP1;
    public GameObject F12EP2;
    public GameObject F13EG;
    public GameObject F13EP1;
    public GameObject F13EP2;
    public GameObject F14EG;
    public GameObject F14EP1;
    public GameObject F14EP2;
    public GameObject F15EG;
    public GameObject F15EP1;
    public GameObject F15EP2;

    //testes lokos
    public int nivelHub;

    void Start()
    {
        int estrelas;

        //teste separar estrelas
        int estrelasM;

        //aqui também teste loko
        nivelHub = PlayerPrefs.GetInt("nivel");

        this.carregaSkin();

        if (nivelHub == 1)
        {

            if (PlayerPrefs.HasKey("estrelas1"))
            {
                estrelas = PlayerPrefs.GetInt("estrelas1");
                //habilita as estrelas da fase 1
                if (estrelas == 1)
                {
                    EG.SetActive(false);
                    EP1.SetActive(true);
                    EP2.SetActive(false);
                }
                else if (estrelas == 2)
                {
                    EG.SetActive(false);
                    EP1.SetActive(true);
                    EP2.SetActive(true);
                }
                else if (estrelas == 3)
                {
                    EG.SetActive(true);
                    EP1.SetActive(true);
                    EP2.SetActive(true);
                    FASE2ATIVA.SetActive(true);
                    FASE2INATIVA.SetActive(false);
                }




                if (PlayerPrefs.HasKey("estrelas2"))
                {
                    estrelas = PlayerPrefs.GetInt("estrelas2");
                    //habilita as estrelas da fase 1
                    if (estrelas == 1)
                    {
                        F2EG.SetActive(false);
                        F2EP1.SetActive(true);
                        F2EP2.SetActive(false);
                    }
                    else if (estrelas == 2)
                    {
                        F2EG.SetActive(false);
                        F2EP1.SetActive(true);
                        F2EP2.SetActive(true);
                    }
                    else if (estrelas == 3)
                    {
                        F2EG.SetActive(true);
                        F2EP1.SetActive(true);
                        F2EP2.SetActive(true);
                        FASE3ATIVA.SetActive(true);
                        FASE3INATIVA.SetActive(false);
                    }


                }

                if (PlayerPrefs.HasKey("estrelas3"))
                {
                    estrelas = PlayerPrefs.GetInt("estrelas3");
                    FASE3ATIVA.SetActive(true);
                    FASE3INATIVA.SetActive(false);
                    //habilita as estrelas da fase 1
                    if (estrelas == 1)
                    {
                        F3EG.SetActive(false);
                        F3EP1.SetActive(true);
                        F3EP2.SetActive(false);
                    }
                    else if (estrelas == 2)
                    {
                        F3EG.SetActive(false);
                        F3EP1.SetActive(true);
                        F3EP2.SetActive(true);
                    }
                    else if (estrelas == 3)
                    {
                        F3EG.SetActive(true);
                        F3EP1.SetActive(true);
                        F3EP2.SetActive(true);
                        FASE4ATIVA.SetActive(true);
                        FASE4INATIVA.SetActive(false);
                    }


                }

                if (PlayerPrefs.HasKey("estrelas4"))
                {
                    estrelas = PlayerPrefs.GetInt("estrelas4");
                    FASE4ATIVA.SetActive(true);
                    FASE4INATIVA.SetActive(false);
                    //habilita as estrelas da fase 1
                    if (estrelas == 1)
                    {
                        F4EG.SetActive(false);
                        F4EP1.SetActive(true);
                        F4EP2.SetActive(false);
                    }
                    else if (estrelas == 2)
                    {
                        F4EG.SetActive(false);
                        F4EP1.SetActive(true);
                        F4EP2.SetActive(true);
                    }
                    else if (estrelas == 3)
                    {
                        F4EG.SetActive(true);
                        F4EP1.SetActive(true);
                        F4EP2.SetActive(true);
                        FASE5ATIVA.SetActive(true);
                        FASE5INATIVA.SetActive(false);
                    }


                }

                if (PlayerPrefs.HasKey("estrelas5"))
                {
                    estrelas = PlayerPrefs.GetInt("estrelas5");
                    FASE5ATIVA.SetActive(true);
                    FASE5INATIVA.SetActive(false);
                    //habilita as estrelas da fase 1
                    if (estrelas == 1)
                    {
                        F5EG.SetActive(false);
                        F5EP1.SetActive(true);
                        F5EP2.SetActive(false);
                    }
                    else if (estrelas == 2)
                    {
                        F5EG.SetActive(false);
                        F5EP1.SetActive(true);
                        F5EP2.SetActive(true);
                    }
                    else if (estrelas == 3)
                    {
                        F5EG.SetActive(true);
                        F5EP1.SetActive(true);
                        F5EP2.SetActive(true);

                    }
                }
            }
        }

        //teste estrelas por nivel de dificuldade
        //MÉDIO 6-10
        //NIVEL ADIÇÃO DIFICULDADE MÉDIO

        if (nivelHub == 2)
        {
            if (PlayerPrefs.HasKey("estrelas6"))
            {
                estrelas = PlayerPrefs.GetInt("estrelas6");
                //habilita as estrelas da fase 1
                if (estrelas == 1)
                {
                    EG.SetActive(false);
                    EP1.SetActive(true);
                    EP2.SetActive(false);
                }
                else if (estrelas == 2)
                {
                    EG.SetActive(false);
                    EP1.SetActive(true);
                    EP2.SetActive(true);
                }
                else if (estrelas == 3)
                {
                    EG.SetActive(true);
                    EP1.SetActive(true);
                    EP2.SetActive(true);
                    FASE2ATIVA.SetActive(true);
                    FASE2INATIVA.SetActive(false);
                }

            }

            //NIVEL SUBTRAÇÃO DIFICULDADE MÉDIO
            if (PlayerPrefs.HasKey("estrelas7"))
            {
                estrelas = PlayerPrefs.GetInt("estrelas7");
                //habilita as estrelas da fase 1
                if (estrelas == 1)
                {
                    F2EG.SetActive(false);
                    F2EP1.SetActive(true);
                    F2EP2.SetActive(false);
                }
                else if (estrelas == 2)
                {
                    F2EG.SetActive(false);
                    F2EP1.SetActive(true);
                    F2EP2.SetActive(true);
                }
                else if (estrelas == 3)
                {
                    F2EG.SetActive(true);
                    F2EP1.SetActive(true);
                    F2EP2.SetActive(true);
                    FASE3ATIVA.SetActive(true);
                    FASE3INATIVA.SetActive(false);
                }


            }

            //NIVEL MULTIPLICAÇÃO DIFICULDADE MÉDIO
            if (PlayerPrefs.HasKey("estrelas8"))
            {
                estrelas = PlayerPrefs.GetInt("estrelas8");
                FASE3ATIVA.SetActive(true);
                FASE3INATIVA.SetActive(false);
                //habilita as estrelas da fase 1
                if (estrelas == 1)
                {
                    F3EG.SetActive(false);
                    F3EP1.SetActive(true);
                    F3EP2.SetActive(false);
                }
                else if (estrelas == 2)
                {
                    F3EG.SetActive(false);
                    F3EP1.SetActive(true);
                    F3EP2.SetActive(true);
                }
                else if (estrelas == 3)
                {
                    F3EG.SetActive(true);
                    F3EP1.SetActive(true);
                    F3EP2.SetActive(true);
                    FASE4ATIVA.SetActive(true);
                    FASE4INATIVA.SetActive(false);
                }


            }

            //NIVEL DIVISÃO DIFICULDADE MÉDIO
            if (PlayerPrefs.HasKey("estrelas9"))
            {
                estrelas = PlayerPrefs.GetInt("estrelas9");
                FASE4ATIVA.SetActive(true);
                FASE4INATIVA.SetActive(false);
                //habilita as estrelas da fase 1
                if (estrelas == 1)
                {
                    F4EG.SetActive(false);
                    F4EP1.SetActive(true);
                    F4EP2.SetActive(false);
                }
                else if (estrelas == 2)
                {
                    F4EG.SetActive(false);
                    F4EP1.SetActive(true);
                    F4EP2.SetActive(true);
                }
                else if (estrelas == 3)
                {
                    F4EG.SetActive(true);
                    F4EP1.SetActive(true);
                    F4EP2.SetActive(true);
                    FASE5ATIVA.SetActive(true);
                    FASE5INATIVA.SetActive(false);
                }


            }

            //NIVEL FINAL DIFICULDADE MÉDIO
            if (PlayerPrefs.HasKey("estrelas10"))
            {
                estrelas = PlayerPrefs.GetInt("estrelas10");
                FASE5ATIVA.SetActive(true);
                FASE5INATIVA.SetActive(false);
                //habilita as estrelas da fase 1
                if (estrelas == 1)
                {
                    F5EG.SetActive(false);
                    F5EP1.SetActive(true);
                    F5EP2.SetActive(false);
                }
                else if (estrelas == 2)
                {
                    F5EG.SetActive(false);
                    F5EP1.SetActive(true);
                    F5EP2.SetActive(true);
                }
                else if (estrelas == 3)
                {
                    F5EG.SetActive(true);
                    F5EP1.SetActive(true);
                    F5EP2.SetActive(true);

                }
            }
        }
        //DIFÍCIL
        if (nivelHub == 3)
        {
            if (PlayerPrefs.HasKey("estrelas11"))
            {
                estrelas = PlayerPrefs.GetInt("estrelas11");
                //habilita as estrelas da fase 1
                if (estrelas == 1)
                {
                    F11EG.SetActive(false);
                    F11EP1.SetActive(true);
                    F11EP2.SetActive(false);
                }
                else if (estrelas == 2)
                {
                    F11EG.SetActive(false);
                    F11EP1.SetActive(true);
                    F11EP2.SetActive(true);
                }
                else if (estrelas == 3)
                {
                    F11EG.SetActive(true);
                    F11EP1.SetActive(true);
                    F11EP2.SetActive(true);
                    FASE2ATIVA.SetActive(true);
                    FASE2INATIVA.SetActive(false);
                }


            }

            //NIVEL SUBTRAÇÃO DIFICULDADE DIFÍCIL
            if (PlayerPrefs.HasKey("estrelas12"))
            {
                estrelas = PlayerPrefs.GetInt("estrelas12");
                //habilita as estrelas da fase 1
                if (estrelas == 1)
                {
                    F12EG.SetActive(false);
                    F12EP1.SetActive(true);
                    F12EP2.SetActive(false);
                }
                else if (estrelas == 2)
                {
                    F12EG.SetActive(false);
                    F12EP1.SetActive(true);
                    F12EP2.SetActive(true);
                }
                else if (estrelas == 3)
                {
                    F12EG.SetActive(true);
                    F12EP1.SetActive(true);
                    F12EP2.SetActive(true);
                    FASE3ATIVA.SetActive(true);
                    FASE3INATIVA.SetActive(false);
                }


            }

            //NIVEL MULTIPLICAÇÃO DIFICULDADE DIFÍCIL
            if (PlayerPrefs.HasKey("estrelas13"))
            {
                estrelas = PlayerPrefs.GetInt("estrelas13");
                FASE3ATIVA.SetActive(true);
                FASE3INATIVA.SetActive(false);
                //habilita as estrelas da fase 1
                if (estrelas == 1)
                {
                    F13EG.SetActive(false);
                    F13EP1.SetActive(true);
                    F13EP2.SetActive(false);
                }
                else if (estrelas == 2)
                {
                    F13EG.SetActive(false);
                    F13EP1.SetActive(true);
                    F13EP2.SetActive(true);
                }
                else if (estrelas == 3)
                {
                    F13EG.SetActive(true);
                    F13EP1.SetActive(true);
                    F13EP2.SetActive(true);
                    FASE4ATIVA.SetActive(true);
                    FASE4INATIVA.SetActive(false);
                }


            }
        }

        //NIVEL DIVISÃO DIFICULDADE DIFÍCIL
        if (PlayerPrefs.HasKey("estrelas14"))
        {
            estrelas = PlayerPrefs.GetInt("estrelas14");
            FASE4ATIVA.SetActive(true);
            FASE4INATIVA.SetActive(false);
            //habilita as estrelas da fase 1
            if (estrelas == 1)
            {
                F14EG.SetActive(false);
                F14EP1.SetActive(true);
                F14EP2.SetActive(false);
            }
            else if (estrelas == 2)
            {
                F14EG.SetActive(false);
                F14EP1.SetActive(true);
                F14EP2.SetActive(true);
            }
            else if (estrelas == 3)
            {
                F14EG.SetActive(true);
                F14EP1.SetActive(true);
                F14EP2.SetActive(true);
                FASE5ATIVA.SetActive(true);
                FASE5INATIVA.SetActive(false);
            }


        }

        //NIVEL FINAL DIFICULDADE DIFÍCIL
        if (PlayerPrefs.HasKey("estrelas15"))
        {
            estrelas = PlayerPrefs.GetInt("estrelas15");
            FASE5ATIVA.SetActive(true);
            FASE5INATIVA.SetActive(false);
            //habilita as estrelas da fase 1
            if (estrelas == 1)
            {
                F15EG.SetActive(false);
                F15EP1.SetActive(true);
                F15EP2.SetActive(false);
            }
            else if (estrelas == 2)
            {
                F15EG.SetActive(false);
                F15EP1.SetActive(true);
                F15EP2.SetActive(true);
            }
            else if (estrelas == 3)
            {
                F15EG.SetActive(true);
                F15EP1.SetActive(true);
                F15EP2.SetActive(true);

            }
        }


    }

    

	//usado na escolha das FASES
	public void chamaOperacao(string operacao)
	{

		PlayerPrefs.SetString ("op", operacao);

	}

	private void carregaSkin()
	{

        string caminho = "Fases/" + PlayerPrefs.GetString ("skin") + "/";
       
        botao.GetComponent<Image>().sprite = Resources.Load<Sprite> (caminho+"1");
		FASE2ATIVA.GetComponent<Image>().sprite = Resources.Load<Sprite> (caminho+"2");
		FASE2INATIVA.GetComponent<Image>().sprite = Resources.Load<Sprite> (caminho+"2L");
		FASE3ATIVA.GetComponent<Image>().sprite = Resources.Load<Sprite> (caminho+"3");
		FASE3INATIVA.GetComponent<Image>().sprite = Resources.Load<Sprite> (caminho+"3L");
		FASE4ATIVA.GetComponent<Image>().sprite = Resources.Load<Sprite> (caminho+"4");
		FASE4INATIVA.GetComponent<Image>().sprite = Resources.Load<Sprite> (caminho+"4L");
		FASE5ATIVA.GetComponent<Image>().sprite = Resources.Load<Sprite> (caminho+"5");
		FASE5INATIVA.GetComponent<Image>().sprite = Resources.Load<Sprite> (caminho+"5L");
		NOMEFASE.GetComponent<Image>().sprite = Resources.Load<Sprite>(caminho + "Nome");

    }



}
