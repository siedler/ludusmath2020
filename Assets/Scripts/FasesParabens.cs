﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Personalizacao;
using System;

public class FasesParabens : MonoBehaviour {

    public GameObject EG;
    public GameObject EP1;
    public GameObject EP2;
    public GameObject EGoff;
    public GameObject EP1off;
    public GameObject EP2off;
    public int valor;
    public String skin;
    public int estrelasM;

    public int nivelHub;
    void Start()
    {
        nivelHub = PlayerPrefs.GetInt("nivel");
        if (nivelHub == 0)
        {
            nivelHub = 1;
        }

        //Carrega n° de estrelas conquistados
        estrelasM = Pergunta.estrelas;

            if (estrelasM == 1)
            {
                EG.SetActive(false);
                EP1.SetActive(true);
                EP2.SetActive(false);

                EGoff.SetActive(true);
                EP1off.SetActive(false);
                EP2off.SetActive(true);
        }
            else if (estrelasM == 2)
            {
                EG.SetActive(false);
                EP1.SetActive(true);
                EP2.SetActive(true);

                EGoff.SetActive(true);
                EP1off.SetActive(false);
                EP2off.SetActive(false);
        }
            else if (estrelasM == 3)
            {
                EG.SetActive(true);
                EP1.SetActive(true);
                EP2.SetActive(true);

                EGoff.SetActive(false);
                EP1off.SetActive(false);
                EP2off.SetActive(false);

        }

        }


    
   
    public void AvancarParabens()
    {

        
        Skin.Carregar();
        skin = "hubFases" + nivelHub;
        Debug.Log(skin);
        SceneManager.LoadScene(skin);
    }
    
}


