﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Loja : MonoBehaviour
{
    public Canvas Canvas;

    private Moeda din;
    public GameObject faltaMoedas;

    public GameObject panelComprarEgito;
    public GameObject faseEgitoAtiva;
    public GameObject faseEgitoInativa;
   

    public GameObject panelComprarFadas;
    public GameObject faseFadasAtiva;
    public GameObject faseFadasInativa;

    public GameObject panelComprarSereias;
    public GameObject faseSereiasAtiva;
    public GameObject faseSereiasInativa;

    public GameObject panelComprarOrcs;
    public GameObject faseOrcsAtiva;
    public GameObject faseOrcsInativa;
    

   
    private bool telaComprarEgito;
    private bool telafaseEgitoAtiva;
    private bool telafaseEgitoInativa;
    private bool telafaltaMoedas;

    private bool telaComprarFadas;
    private bool telafaseFadasAtiva;
    private bool telafaseFadasInativa;

    private bool telaComprarSereias;
    private bool telafaseSereiasAtiva;
    private bool telafaseSereiasInativa;

    private bool telaComprarOrcs;
    private bool telafaseOrcsAtiva;
    private bool telafaseOrcsInativa;


    public int a, b, c, d;
    
    public Text dinText;

    void Start()
    {
        din = FindObjectOfType(typeof(Moeda)) as Moeda;
        if (a < PlayerPrefs.GetInt("a"))
        {
            faseEgitoAtiva.SetActive(true);
            panelComprarEgito.SetActive(false);
            
        }
        if (b < PlayerPrefs.GetInt("b"))
        {
            faseFadasAtiva.SetActive(true);
            panelComprarFadas.SetActive(false);
        }
        if (c < PlayerPrefs.GetInt("c"))
        {
            faseSereiasAtiva.SetActive(true);
            panelComprarSereias.SetActive(false);
        }
        if (d < PlayerPrefs.GetInt("d"))
        {
            faseOrcsAtiva.SetActive(true);
            panelComprarOrcs.SetActive(false);
        }

    }

    public void abrirPainelEgito() {

        if (telaComprarEgito == false)
        {
            panelComprarEgito.SetActive(true);
            telaComprarEgito = true;
        }
        else if (telaComprarEgito == true)
        {
            panelComprarEgito.SetActive(false);
            telaComprarEgito = false;

        }


    }

    public void comprarEgito() {
        if (din.dinheiro >= 40)
        {
            panelComprarEgito.SetActive(false);
            telaComprarEgito = false;
            faseEgitoAtiva.SetActive(true);
            din.dinheiro = din.dinheiro - 40;
            dinText.text = din.dinheiro.ToString();
            PlayerPrefs.SetInt("moedas", din.dinheiro);
            Debug.Log(din.dinheiro);
            PlayerPrefs.SetString("skin", "Egito");
            PlayerPrefs.SetInt("a", 1);
        }

        else
        {
            panelComprarEgito.SetActive(false);
            telaComprarEgito = false;
            telafaltaMoedas = true;
            faltaMoedas.SetActive(true);
        }
       
    }

     public void abrirPainelFadas() {

        if (telaComprarFadas == false)
        {
            panelComprarFadas.SetActive(true);
            telaComprarFadas = true;
        }
        else if (telaComprarFadas == true)
        {
            panelComprarFadas.SetActive(false);
            telaComprarFadas = false;

        }

    }

    public void comprarFadas() {
        if (din.dinheiro >= 70)
        {
            panelComprarFadas.SetActive(false);
            telaComprarFadas = false;
            faseFadasAtiva.SetActive(true);
            din.dinheiro = din.dinheiro - 70;
            dinText.text = din.dinheiro.ToString();
            PlayerPrefs.SetInt("moedas", din.dinheiro);
            Debug.Log(din.dinheiro);
            PlayerPrefs.SetString("skin", "Fadas");
            PlayerPrefs.SetInt("b", 1);
        }
        else {
            panelComprarFadas.SetActive(false);
            telaComprarFadas = false;
            telafaltaMoedas = true;
            faltaMoedas.SetActive(true);
            //b = 0;
        }
       
    }


    public void abrirPainelSereias()
    {

        if (telaComprarSereias == false)
        {
            panelComprarSereias.SetActive(true);
            telaComprarSereias = true;
        }
        else if (telaComprarSereias == true)
        {
            panelComprarSereias.SetActive(false);
            telaComprarSereias = false;

        }

    }

    public void comprarSereias()
    {
        if (din.dinheiro >= 90)
        {
            panelComprarSereias.SetActive(false);
            telaComprarSereias = false;
            faseSereiasAtiva.SetActive(true);
            din.dinheiro = din.dinheiro - 90;
            dinText.text = din.dinheiro.ToString();
            PlayerPrefs.SetInt("moedas", din.dinheiro);
            Debug.Log(din.dinheiro);
            PlayerPrefs.SetString("skin", "Sereias");
            PlayerPrefs.SetInt("c", 1);
        }
        else
        {
            panelComprarSereias.SetActive(false);
            telaComprarSereias = false;
            telafaltaMoedas = true;
            faltaMoedas.SetActive(true);   
        }

    }

    public void abrirPainelOrcs()
    {

        if (telaComprarOrcs == false)
        {
            panelComprarOrcs.SetActive(true);
            telaComprarOrcs = true;
        }
        else if (telaComprarOrcs == true)
        {
            panelComprarOrcs.SetActive(false);
            telaComprarOrcs = false;


        }

    }

    public void comprarOrcs()
    {
        if (din.dinheiro >= 100)
        {
            panelComprarOrcs.SetActive(false);
            telaComprarOrcs = false;
            faseOrcsAtiva.SetActive(true);
            din.dinheiro = din.dinheiro - 100;
            dinText.text = din.dinheiro.ToString();
            PlayerPrefs.SetInt("moedas", din.dinheiro);
            Debug.Log(din.dinheiro);
            PlayerPrefs.SetString("skin", "Orcs");
            PlayerPrefs.SetInt("d", 1);
        }
        else
        {
            panelComprarOrcs.SetActive(false);
            telaComprarOrcs = false;
            telafaltaMoedas = true;
            faltaMoedas.SetActive(true);
        }

    }


    public void fecharAviso() {
        faltaMoedas.SetActive(false);
    }

    public void fecharPainel() {
        panelComprarEgito.SetActive(false);
        panelComprarFadas.SetActive(false);
        panelComprarSereias.SetActive(false);
        panelComprarOrcs.SetActive(false);
    }
     
}
