﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
//GIT
using System;
using Personalizacao;


public class Pergunta : MonoBehaviour
{

    public Animator menino;
    public Animator galina;
    public Animator galina2;
    public Animator galina3;

    private Text pergunta;
    public Text b1;
    public Text b2;
    public Text b3;
    //  public Text n;

    public int idPergunta;
    public string questoes;
    public int respA;
    public int respB;
    public int respC;
    public int certa;
    public PerguntaLudus q;

    public int nivel;
    private Moeda din;
    public int tenta;

    //troca personagem
    //public escolherPersonagem SkinPersonagem;

    [SerializeField]
    private Button[] Personagens;
    private int AnimacoesPersonagens = 0;

    public bool proxima;

    public static float velocidade; //velocidade do movimento do objeto

    public GameObject g1;
    public GameObject g2;
    public GameObject g3;

    public Galinha galinha1;
    public Galinha galinha2;
    public Galinha galinha3;

    public int pontosOperacao;
    private string operacaoAtual;
    private bool vertical;
    private List<PerguntaLudus> lista;
    //teste troca de personagem
    String Personagem;


    public int nivelHub;




    public Pergunta()
    {


    }



    void Start()
    {


        //testes para carregar animator dos outros personagens

        Personagem = PlayerPrefs.GetString("SkinPersonagem");//vem do script escolher personagem
        RuntimeAnimatorController PersonagemAnimator = Resources.Load("Animator/Personagem/" + Personagem) as RuntimeAnimatorController;
        menino.runtimeAnimatorController = PersonagemAnimator;






        nivelHub = PlayerPrefs.GetInt("nivel");

        din = FindObjectOfType(typeof(Moeda)) as Moeda;
        //verdicia se tem MoedaLojas salvas - se tem atualiza
        idPergunta = 0;
        //depois tirar...só para teste
        Skin.Carregar();


        if (PlayerPrefs.HasKey("op"))
        {
            operacaoAtual = PlayerPrefs.GetString("op");
        }
        else
        {

            operacaoAtual = "+";
        }

        carregarSkin();
        //Aqui tem a definição do nível - pode passar de um a quatro
        //definido no hub de entrada

        if (PlayerPrefs.HasKey("nivel"))
        {
            nivel = PlayerPrefs.GetInt("nivel");
        }
        else
        {

            nivel = 1;
        }
        lista = QuestaoAleatoria.aleatoria(operacaoAtual, nivel);
        q = lista[0];
        lista.RemoveAt(0);
        tenta = 3;
        pontosOperacao = 0;


        //Verifica a orientação antes de exibir o titulo
        GameObject panel = GameObject.Find("Canvas/panelVertical");
        //MARIZELE -- ajuste na orientacao
        if (PlayerPrefs.HasKey("orientacao"))
        {
            if (PlayerPrefs.GetInt("orientacao") == 1)
            {
                vertical = true;
                String[] parte1 = q.titulo.Split(q.filtro.ToCharArray()[0]);
                String[] parte2 = parte1[1].Split('=');
                q.titulo = " " + parte1[0] + "\n" + q.filtro + parte2[0] + "\n " + parte2[1];

                panel.SetActive(true);
                this.pergunta = (Text)GameObject.Find("Canvas/panelVertical/OperacaoVertical").GetComponent<Text>();
            }
            else
            {

                panel.SetActive(false);

                this.pergunta = (Text)GameObject.Find("Canvas/Operacao").GetComponent<Text>();
            }
        }
        else
        {

            panel.SetActive(false);

            this.pergunta = (Text)GameObject.Find("Canvas/Operacao").GetComponent<Text>();
        }

        this.pergunta.text = q.titulo;
        this.b1.text = q.opcoes[0];
        this.b2.text = q.opcoes[1];
        this.b3.text = q.opcoes[2];


        //posicao original -- -46 / 141 / 307


        galinha1 = new Galinha(g1, 200, 31 - 56.0f);
        galinha1.attribuirAnimation(galina);
        galinha2 = new Galinha(g2, 250, 31 + 1f);
        galinha2.attribuirAnimation(galina2);
        galinha3 = new Galinha(g3, 300, 31 + 58f);
        galinha3.attribuirAnimation(galina3);
        velocidade = -1.08f;
        proxima = false;


    }

    public void trocaOperacao()
    {

        comandosBasicos cb = new comandosBasicos();
        int estrelas;

        estrelas = 0;//desconsiderarei temporariamente
        switch (operacaoAtual)
        {

            case "+":



                if (nivelHub == 1)
                {
                    PlayerPrefs.SetInt("estrelas" + (estrelas + 1).ToString(), calcularEstrelas());
                    //operacaoAtual = "-";
                }

                if (nivelHub == 2)
                {
                    PlayerPrefs.SetInt("estrelas" + (estrelas + 6).ToString(), calcularEstrelas());
                }

                if (nivelHub == 3)
                {
                    PlayerPrefs.SetInt("estrelas" + (estrelas + 11).ToString(), calcularEstrelas());
                }

                cb.carregaCena("Fim");
                break;
            case "-":

                if (nivelHub == 1)
                {
                    PlayerPrefs.SetInt("estrelas" + (estrelas + 2).ToString(), calcularEstrelas());
                    //operacaoAtual = "*";
                }

                if (nivelHub == 2)
                {
                    PlayerPrefs.SetInt("estrelas" + (estrelas + 7).ToString(), calcularEstrelas());
                }

                if (nivelHub == 3)
                {
                    PlayerPrefs.SetInt("estrelas" + (estrelas + 12).ToString(), calcularEstrelas());
                }

                cb.carregaCena("Fim");
                break;
            case "*":
                if (nivelHub == 1)
                {
                    PlayerPrefs.SetInt("estrelas" + (estrelas + 3).ToString(), calcularEstrelas());
                    //operacaoAtual = "/";
                }

                if (nivelHub == 2)
                {
                    PlayerPrefs.SetInt("estrelas" + (estrelas + 8).ToString(), calcularEstrelas());
                }

                if (nivelHub == 3)
                {
                    PlayerPrefs.SetInt("estrelas" + (estrelas + 13).ToString(), calcularEstrelas());
                }

                cb.carregaCena("Fim");
                break;

            case "÷":
                if (nivelHub == 1)
                {
                    PlayerPrefs.SetInt("estrelas" + (estrelas + 4).ToString(), calcularEstrelas());
                    //operacaoAtual = "%";
                }

                if (nivelHub == 2)
                {
                    PlayerPrefs.SetInt("estrelas" + (estrelas + 9).ToString(), calcularEstrelas());
                }

                if (nivelHub == 3)
                {
                    PlayerPrefs.SetInt("estrelas" + (estrelas + 14).ToString(), calcularEstrelas());
                }
                cb.carregaCena("Fim");
                break;
            case "%":
                if (nivelHub == 1)
                {
                    PlayerPrefs.SetInt("estrelas" + (estrelas + 5).ToString(), calcularEstrelas());
                    //operacaoAtual = null;
                    //perguntas = misturado;
                }

                if (nivelHub == 2)
                {
                    PlayerPrefs.SetInt("estrelas" + (estrelas + 10).ToString(), calcularEstrelas());
                }

                if (nivelHub == 3)
                {
                    PlayerPrefs.SetInt("estrelas" + (estrelas + 15).ToString(), calcularEstrelas());
                }
                cb.carregaCena("Parabens");
                break;

            default:
                break;
        }




    }

    void proximaPergunta()
    {

        idPergunta += 1;

        //verifica se é fim de jogo

        if (idPergunta == 5)
        {
            trocaOperacao();
            proxima = false;
        }
        else
        {
            proxima = true;
            tenta = 3;
            q = lista[0];
            lista.RemoveAt(0);
        }





    }

    public void exibeTitulo()
    {
        pergunta.text = q.titulo;
    }


    public void verificaResposta(string alternativa)
    {

        if (galinha1.parado)
        {
            menino.SetBool("erra", false);
            menino.SetBool("erra2", false);
            menino.SetBool("acerta", false);
            bool certa;
            int valor = 1;
            switch (alternativa)
            {
                case "A":
                    if (q.certa == alternativa)
                    {

                        din.adicionaValor(valor * tenta);
                        menino.SetBool("acerta", true);
                        //grava o valor das moedas
                        PlayerPrefs.SetInt("moedas", din.dinheiro);
                        PlayerPrefs.Save();
                        pontosOperacao += valor * tenta;
                        proximaPergunta();
                        galinha1.setMovimento("E");
                        galinha2.setMovimento("D");
                        galinha3.setMovimento("D");
                        certa = true;//aquiiiiii
                                     //aqui a animacao que deu certo

                    }
                    else
                    {
                        //aqui a animacao que deu errado
                        animaErro();

                        if (tenta != 1)
                        {
                            tenta = tenta - 1;
                        }
                    }
                    break;
                case "B":
                    if (q.certa == alternativa)
                    {
                        din.adicionaValor(valor * tenta);
                        menino.SetBool("acerta", true);
                        PlayerPrefs.SetInt("moedas", din.dinheiro);
                        PlayerPrefs.Save();
                        pontosOperacao += valor * tenta;
                        proximaPergunta();
                        certa = true;
                        galinha1.setMovimento("D");
                        galinha2.setMovimento("E");
                        galinha3.setMovimento("D");

                    }
                    else
                    {
                        animaErro();
                        if (tenta != 1)
                        {
                            tenta = tenta - 1;
                        }
                    }
                    break;
                case "C":
                    if (q.certa == alternativa)
                    {
                        din.adicionaValor(valor * tenta);
                        menino.SetBool("acerta", true);
                        PlayerPrefs.SetInt("moedas", din.dinheiro);
                        PlayerPrefs.Save();
                        pontosOperacao += valor * tenta;
                        proximaPergunta();
                        certa = true;
                        galinha1.setMovimento("D");
                        galinha2.setMovimento("D");
                        galinha3.setMovimento("E");

                    }
                    else
                    {
                        animaErro();
                        if (tenta != 1)
                        {
                            tenta = tenta - 1;
                        }
                    }
                    break;
            }


        }
    }


    // Update is called once per frame
    void FixedUpdate()
    {

        galinha1.andarGalinha();
        galinha2.andarGalinha();
        galinha3.andarGalinha();

        if (galinha1.parado && galinha2.parado && galinha3.parado)
        {
            if (proxima)
            {
                //Verifica a orientação antes de exibir o titulo
                if (vertical)
                {
                    String[] parte1 = q.titulo.Split(q.filtro.ToCharArray()[0]);
                    String[] parte2 = parte1[1].Split('=');
                    q.titulo = " " + parte1[0] + "\n" + q.filtro + parte2[0] + "\n " + parte2[1];
                }
                this.pergunta.text = q.titulo;
                this.b1.text = q.opcoes[0];
                this.b2.text = q.opcoes[1];
                this.b3.text = q.opcoes[2];
                proxima = false;
                menino.SetBool("acerta", false);
                galinha1.setMovimento("I");
                galinha2.setMovimento("I");
                galinha3.setMovimento("I");

            }

        }

    }
    public static int estrelas;
    private int calcularEstrelas()
    {


        if (pontosOperacao >= 13)
        {
            estrelas = 3;
        }
        else if (pontosOperacao >= 8)
        {
            estrelas = 2;
        }
        else
        {
            estrelas = 1;
        }
        pontosOperacao = 0;
        return estrelas;

    }

    private void animaErro()
    {
        switch (tenta)
        {

            case 3:
                menino.SetBool("erra", true);
                break;
            case 2:
                menino.SetBool("erra2", true);
                break;
            case 1:
                menino.SetBool("erra", true);
                break;

            default:
                break;
        }
    }

    private void carregarSkin()
    {
        int numero;
        switch (operacaoAtual)
        {
            case "+":
                numero = 0;
                break;
            case "-":
                numero = 1;
                break;
            case "÷":
                numero = 2;
                break;
            case "*":
                numero = 3;
                break;
            case "%":
                numero = 4;
                break;
            default:
                numero = 0;//aqui da pra pegar aleatorio 
                break;
        }

        //Busca a fase correspondente
        Fase f = Skin.fases[numero];
        //Marizele
        //Se for fase final faz mistura de animais
        String caminhoAnimator;
        caminhoAnimator = "Animator/" + PlayerPrefs.GetString("skin") + "/";
        if (f.animator == "5")
        {
            System.Random rnd = new System.Random();
            RuntimeAnimatorController ra1 = Resources.Load(caminhoAnimator + rnd.Next(1, 4)) as RuntimeAnimatorController;
            RuntimeAnimatorController ra2 = Resources.Load(caminhoAnimator + +rnd.Next(1, 4)) as RuntimeAnimatorController;
            RuntimeAnimatorController ra3 = Resources.Load(caminhoAnimator + +rnd.Next(1, 4)) as RuntimeAnimatorController;

            galina.runtimeAnimatorController = ra1;
            galina2.runtimeAnimatorController = ra2;
            galina3.runtimeAnimatorController = ra3;

        }
        else
        {
            //se for a fase tradicional chama o animator
            RuntimeAnimatorController ra = Resources.Load(caminhoAnimator + f.animator) as RuntimeAnimatorController;
            galina.runtimeAnimatorController = ra;
            galina2.runtimeAnimatorController = ra;
            galina3.runtimeAnimatorController = ra;
        }


        //b1.transform.position = new Vector3(f.bt1X,f.btY,b1.transform.position.z);
        //b2.transform.position = new Vector3(f.bt2X,f.btY,b2.transform.position.z);
        //b3.transform.position = new Vector3(f.bt3X,f.btY,b3.transform.position.z);




        Color myColor = new Color();
        ColorUtility.TryParseHtmlString(f.corchao, out myColor);

        GameObject.Find("Canvas/Chao/c1").GetComponent<Image>().color = myColor;
        GameObject.Find("Canvas/Chao/c2").GetComponent<Image>().color = myColor;
        GameObject.Find("Canvas/Chao/c3").GetComponent<Image>().color = myColor;
        GameObject.Find("Canvas/Chao/c4").GetComponent<Image>().color = myColor;
        GameObject.Find("Canvas/Chao/c5").GetComponent<Image>().color = myColor;
        GameObject.Find("Canvas/Chao/c6").GetComponent<Image>().color = myColor;
        GameObject.Find("Canvas/Chao/c7").GetComponent<Image>().color = myColor;

        Image cenario = GameObject.Find("Canvas/Fundo").GetComponent<Image>();


        cenario.sprite = Resources.Load<Sprite>("Cenario/" + PlayerPrefs.GetString("skin") + "/" + f.cenario.imagem);
        RectTransform rt = (RectTransform)cenario.transform;

        cenario.transform.position = new Vector3(f.cenario.posicaox, cenario.transform.position.y, cenario.transform.position.z);
    }
}