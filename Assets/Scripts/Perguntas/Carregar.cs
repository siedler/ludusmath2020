﻿using System;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;


public class Carregar
{
	private String arquivo;

	public Carregar (String arquivoJson)
	{
		this.arquivo = arquivoJson;
	}

	public  List<PerguntaLudus> carregarPerguntas (int numero, String filtro)
	{
			
		TextAsset targetFile = Resources.Load<TextAsset> ("Json/" + this.arquivo);
		List<PerguntaLudus> perguntas = new List<PerguntaLudus>();
		try {
			JObject o = JObject.Parse (targetFile.text);
			IEnumerable<JToken> lista = o.SelectTokens ("$.perguntas[?(@.filtro == '"+filtro+ "')]").OrderBy(x => Guid.NewGuid()).Take(numero);

			foreach (JToken item in lista) {
				PerguntaLudus p = item.ToObject<PerguntaLudus> ();
				perguntas.Add(p);

			}
		} catch (Exception ex) {
			Debug.Log (ex.InnerException);
			return null;


		}
		return perguntas;

	}

	public  List<PerguntaLudus> carregarPerguntas (int numero, String filtro, String dificuldade)
	{

		TextAsset targetFile = Resources.Load<TextAsset> ("Json/" + this.arquivo);
		List<PerguntaLudus> perguntas = new List<PerguntaLudus>();

		try {
			JObject o = JObject.Parse (targetFile.text);
			IEnumerable<JToken> lista ;
			if(!filtro.Equals("%"))
			{
				lista = o.SelectTokens ("$.perguntas[?(@.filtro == '"+filtro+ "' && @.dificuldade == '"+dificuldade+"')]").OrderBy(x => Guid.NewGuid()).Take(numero);
			}
			else{
				lista = o.SelectTokens ("$.perguntas[?(@.dificuldade == '"+dificuldade+"')]").OrderBy(x => Guid.NewGuid()).Take(numero);
					
			}

			foreach (JToken item in lista) {
				PerguntaLudus p = item.ToObject<PerguntaLudus> ();
				perguntas.Add(p);

			}
		} catch (Exception ex) {
			Debug.Log (ex.InnerException);


		}
		return perguntas;
	}

}


