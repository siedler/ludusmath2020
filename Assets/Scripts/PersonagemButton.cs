﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
//GIT
using System;
using Personalizacao;


public class PersonagemButton: MonoBehaviour
{
    [SerializeField]
    private Button[] Personagens;
    private int AnimacoesPersonagens = 0;

    public void SetButton(int AnimacoesPersonagens)
    {
        Personagens[AnimacoesPersonagens].enabled = false;
        AnimacoesPersonagens++;
        Personagens[AnimacoesPersonagens].enabled = true;
    }
}
