﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Personagem : MonoBehaviour
{

	public Dropdown dpdGenero,dpdEtnia;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void atualizarEtnia(){
		PlayerPrefs.SetInt ("Etnia", dpdEtnia.value);
	}

	public void atualizarGenero(){
		PlayerPrefs.SetInt ("Genero", dpdGenero.value);
	}
	public void voltar(){
		UnityEngine.SceneManagement.SceneManager.LoadScene("Home");
	}
}

