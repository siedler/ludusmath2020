﻿using System;

using UnityEngine.UI;
using System.Collections.Generic;

namespace Personalizacao
{
	public class Skin
	{
		public static List<Fase> fases;
		public static void Carregar()
		{
			fases = new List<Fase> ();
			//fase 1
			Fase f = new Fase ();
			f.animator = "1";
			f.cenario = new Cenario ("cenario1", 100);
			f.chao = "2";
			f.bt1X = 188f;
			f.bt2X = 257f;
			f.bt3X = 320f;
			f.btY = -55f;
			f.corchao = "#FFFFFFFF";

			fases.Add (f);
			f = new Fase ();
			f.animator = "2";
			f.cenario = new Cenario ("cenario1", 100);
			f.chao = "2";
			f.bt1X = 188f;
			f.bt2X = 257f;
			f.bt3X = 320f;
			f.btY = -55f;
			f.corchao = "#FFFFFFFF";

			fases.Add (f);
			f = new Fase ();
			f.animator = "3";
			f.cenario = new Cenario ("cenario2", 100);
			f.chao = "2";
			f.bt1X = 203f;
			f.bt2X = 274f;
			f.bt3X = 341f;
			f.btY = -48f;
			f.corchao = "#FFAA00FF";

			fases.Add (f);
			f = new Fase ();
			f.animator = "4";
			f.cenario = new Cenario ("cenario1", 100);
			f.chao = "2";
			f.bt1X = 188f;
			f.bt2X = 257f;
			f.bt3X = 320f;
			f.btY = -55f;
			f.corchao = "#FFFFFFFF";

			fases.Add (f);
			f = new Fase ();
            //Marizele
			f.animator = "5";
			f.cenario = new Cenario ("cenario3", 100);
			f.chao = "2";
			f.bt1X = 188f;
			f.bt2X = 257f;
			f.bt3X = 320f;
			f.btY = -55f;
			f.corchao = "#FFFFFFFF";

			fases.Add (f);
	
		}


	}



}

