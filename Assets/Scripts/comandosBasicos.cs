﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Personalizacao;
using System;

public class comandosBasicos : MonoBehaviour {
    private Animator controleTela;

	public Animator anim;
    public int nivelHub;
    void Start()
    {
        if (!PlayerPrefs.HasKey("nivel"))
        {
            PlayerPrefs.SetInt("nivel", 1);
        }

        nivelHub = PlayerPrefs.GetInt("nivel");
    }
   

    public void carregaCena(string nomeCena)
    {
        SceneManager.LoadScene(nomeCena);
    }


    public void resetJogo()
    {
        PlayerPrefs.DeleteAll();
       
    }
    public void VoltarInicio() {
        SceneManager.LoadScene("Home");
    }
    
    public void telaParabens()
    {
        anim.SetBool("mudar", true);
    }

    public void abrirSite()
    {
        Application.OpenURL("https://ludus.programatche.net/");
    }

	    public void Animation()
    {
        controleTela = GetComponent<Animator>();
        float time;
        
            time = 5;
        

        
            if (time < 0)
            {
            controleTela.SetBool("Normal", time == 0);
        }
            else
            {
                time -= Time.deltaTime;
            }

    }

    public void EncerrarAtividade()
    {
        Application.Quit();
    }

    public void AbrirArquivo()
    {
        Application.OpenURL("C:/Users/Junior/Documents/GitHub/-ludus/Assets/Resources/Outros/Manual_do_Usuario.pdf");
    }

    
    public void CarregaEgito()
    {
        PlayerPrefs.SetString("skin", "Egito");
        Skin.Carregar();
        SceneManager.LoadScene("hubFases" + nivelHub);
    }

    public void CarregaFazenda()
    {
        PlayerPrefs.SetString("skin", "Fazenda");
        Skin.Carregar();
        SceneManager.LoadScene("hubFases" + nivelHub);
    }

    public void CarregaFadas()
    {
        PlayerPrefs.SetString("skin", "Fadas");
        Skin.Carregar();
        SceneManager.LoadScene("hubFases" + nivelHub);
    }

    public void CarregaSereias()
    {
        PlayerPrefs.SetString("skin", "Sereias");
        Skin.Carregar();
        SceneManager.LoadScene("hubFases" + nivelHub);
    }

    public void CarregaOrcs()
    {
        PlayerPrefs.SetString("skin", "Orcs");
        Skin.Carregar();
        SceneManager.LoadScene("hubFases" + nivelHub);
    }


    //aqui vê qual hub abrir, caso o usuário tenha comprado na loja.
    public void AbriHub()
    {
        if (!PlayerPrefs.HasKey("skin"))
        {
            PlayerPrefs.SetString("skin", "fazenda");
            Skin.Carregar();

        }
        //se não definiu a orientação da pergunta seta horizontal
        if (!PlayerPrefs.HasKey("orientacao"))
        {
            PlayerPrefs.SetInt("orientacao", 0);
        }

        if (nivelHub == 0)
        {
            nivelHub = 1;
        }

        String EstiloJogo;
        EstiloJogo = "Scenes/" + "hubFases/" + "hubFases" + nivelHub;
        Skin.Carregar();
        SceneManager.LoadScene(EstiloJogo);
    }
 
}
