﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
//GIT
using System;
using Personalizacao;

public class escolherPersonagem : MonoBehaviour
{

    public GameObject personagemSprite;

    private bool personagemAtivo;
 

    public int personagem;
    public int cont;
    public Sprite A1;
    public Sprite A2;
    public Sprite A3;
    public Sprite A4;
    public Sprite O1;
    public Sprite O2;
    public Sprite O3;
    public Sprite O4;
    public String SkinPersonagem;

    // Start is called before the first frame update
    void Start()
    {

        //personagemSprite = GameObject.Find("Sprites/personagem/a1");
        //if (!PlayerPrefs.HasKey("personagem"))
        //{
        //    personagem = 1;
        //    gameObject.GetComponent<SpriteRenderer>().sprite = A1;
        //    SkinPersonagem = "Menina1";
        //}

        //else
        //{
        //    EscolherPersonagem();
        //}
        //this.gameObject.GetComponent<SpriteRenderer>().sprite = A1;

        personagem = 1;
        gameObject.GetComponent<SpriteRenderer>().sprite = A1;
        SkinPersonagem = "Menina1";
    }
    public void a1button() {
        personagem = 1;
        gameObject.GetComponent<SpriteRenderer>().sprite = A1;
        
        PlayerPrefs.SetString("SkinPersonagem", "Menina1");
        PlayerPrefs.Save();
        EscolherPersonagem();
        

    }
    public void a2button()
    {
        personagem = 2;
        gameObject.GetComponent<SpriteRenderer>().sprite = A2;
        
        PlayerPrefs.SetString("SkinPersonagem", "Menina2");
        PlayerPrefs.Save();
        EscolherPersonagem();
        
    }
    public void a3button()
    {
        personagem = 3;
        gameObject.GetComponent<SpriteRenderer>().sprite = A3;
        
        PlayerPrefs.SetString("SkinPersonagem", "Menina3");
        PlayerPrefs.Save();
        EscolherPersonagem();
        
    }
    public void a4button()
    {
        personagem = 4;
        gameObject.GetComponent<SpriteRenderer>().sprite = A4;
        
        PlayerPrefs.SetString("SkinPersonagem", "Menina4");
        PlayerPrefs.Save();
        EscolherPersonagem();
        
    }
    public void o1button()
    {
        personagem = 5;
        gameObject.GetComponent<SpriteRenderer>().sprite = O1;
        
        PlayerPrefs.SetString("SkinPersonagem", "Menino1");
        PlayerPrefs.Save();
        EscolherPersonagem();
       
    }
    public void o2button()
    {
        personagem = 6;
        gameObject.GetComponent<SpriteRenderer>().sprite = O2;
        
        PlayerPrefs.SetString("SkinPersonagem", "Menino2");
        PlayerPrefs.Save();
        EscolherPersonagem();
        
    }
    public void o3button()
    {
        personagem = 7;
        gameObject.GetComponent<SpriteRenderer>().sprite = O3;
        
        PlayerPrefs.SetString("SkinPersonagem", "Menino3");
        PlayerPrefs.Save();
        EscolherPersonagem();
        
    }
    public void o4button()
    {
        personagem = 8;
        gameObject.GetComponent<SpriteRenderer>().sprite = O4;
        
        PlayerPrefs.SetString("SkinPersonagem", "Menino4");
        PlayerPrefs.Save();
        EscolherPersonagem();
        
    }

    public void EscolherPersonagem() {
        
        
        
        switch (personagem)
        {
            case 1:
                personagemSprite = GameObject.Find("Sprites/personagem/a1");

                break;
            case 2:
                personagemSprite = GameObject.Find("Sprites/personagem/a2");

                break;
            case 3:
                personagemSprite = GameObject.Find("Sprites/personagem/a3");

                break;
            case 4:
                personagemSprite = GameObject.Find("Sprites/personagem/a4");
                break;
            case 5:
                personagemSprite = GameObject.Find("Sprites/personagem/o1");
                break;
            case 6:
                personagemSprite = GameObject.Find("Sprites/personagem/o2");
                break;
            case 7:
                personagemSprite = GameObject.Find("Sprites/personagem/o3");
                break;
            case 8:
                personagemSprite = GameObject.Find("Sprites/personagem/o4");
                break;
        }
        

    }
}

